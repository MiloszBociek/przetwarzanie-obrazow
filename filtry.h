#ifndef FILTRY_H
#define FILTRY_H

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#include"struct.h"
#include"interface.h"

//Prototypy funkcji znajdujacych sie w pliku "filtry.h"
int negatyw(t_obraz * obraz, t_opcje * wybor);
int progowanie(t_obraz * obraz, t_opcje * wybor);
int konturowanie(t_obraz * obraz, t_opcje * wybor);
int rozmycie_poziome(t_obraz * obraz, t_opcje * wybor);
int rozmycie_pionowe(t_obraz * obraz, t_opcje * wybor);
int konwersja(t_obraz * obraz);
int maska(t_obraz * obraz, t_opcje * wybor);
int R_Histogramu(t_obraz * obraz, t_opcje * wybor);
int zmiana_poziomow(t_obraz * obraz, int kolorA, int kolorB);

#endif
