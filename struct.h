#ifndef STRUCT_H
#define STRUCT_H


//Struktura ktora przechowuje dane o obrazie
typedef struct {
int wym_x, wym_y, odcieni;        //Wymiary oraz liczba odcieni
int ile_kolorow, kolory, RGB;     //Zmienne pomocnicze
int *wybrane_kolory;              //Tablica przechowujaca wybrane kolory
int ***piksele;                   //Glowna tablica z pikselami obrazu (3D)
} t_obraz;      

#endif