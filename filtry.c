#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include "filtry.h"


//Funkcja negatyw
int negatyw(t_obraz * obraz, t_opcje *wybor){
  int i, j, k;         //Zmienne pomocnicze

  for(i = 0; i < obraz->wym_y; i++){
    for(j = 0; j < obraz->wym_x; j++){
      for(k = 0; k < obraz->RGB; k++){
        // Wykonywanie negatywu
        obraz->piksele[i][j][obraz->wybrane_kolory[k]] = obraz->odcieni - obraz->piksele[i][j][obraz->wybrane_kolory[k]];       
      }
    }
  }
  return 1;
}


//Funkcja progowania
int progowanie(t_obraz * obraz, t_opcje * wybor){
  int i, j, k, wartosc_progu;   //Zmienne pomocnicze

  //Konwersja z wartosci 0-100 na 0-obraz->odcieni (zazwyczaj 255)
  wartosc_progu=((wybor->w_progu*(obraz->odcieni))/100);
  
  for(i = 0; i < obraz->wym_y; i++){
    for(j = 0; j < obraz->wym_x; j++){
      for(k = 0; k < obraz->RGB; k++){
        //Progowanie
        if(obraz->piksele[i][j][obraz->wybrane_kolory[k]] <= wartosc_progu){ // L(x,y)<=wartosc_progu
          obraz->piksele[i][j][obraz->wybrane_kolory[k]] = 0;                
        }else{  // L(x,y) > wartosc_progu
          obraz->piksele[i][j][obraz->wybrane_kolory[k]] = obraz->odcieni;         
        }
      }
    }  
  }
  return 1;
}


//Funkcja konturowania
int konturowanie(t_obraz * obraz, t_opcje * wybor){   
  int i, j, k, x;  //Zmienne pomocnicze

  for(i = 0; i < obraz->wym_y; i++){
    for(j = 0; j < obraz->wym_x; j++){
      for(k = 0; k<obraz->RGB; k++){
        //Jezeli nie ma krawedzi
        if(i < obraz->wym_y-1 && j < obraz->wym_x-1){
          x = (abs(obraz->piksele[i][j+1][obraz->wybrane_kolory[k]] - obraz->piksele[i][j][obraz->wybrane_kolory[k]]) + abs(obraz->piksele[i+1][j][obraz->wybrane_kolory[k]] - obraz->piksele[i][j][obraz->wybrane_kolory[k]]));
        }
        else{
          x = 0;    //Na krawedziach obrazu przypisz wartosc 0
        }
        obraz->piksele[i][j][obraz->wybrane_kolory[k]] = x/2;  //Normalizacja danych
      }
    }
  }
  return 1;
}


// Konwersja
int konwersja(t_obraz * obraz){
int i, j, k;

 for(i = 0; i < obraz->wym_y; i++){
    for(j = 0; j < obraz->wym_x; j++){
      for(k = 0; k < 3; k++){
      obraz->piksele[i][j][k] = ((obraz->piksele[i][j][0] + obraz->piksele[i][j][1] + obraz->piksele[i][j][2])/3);  //Konwersja do P2
      }
    }
  }
  return 1;
}

//Funkcja filtru splotu (maski)
int maska(t_obraz * obraz, t_opcje * wybor){
  int i, j, k, Znak = 0;  //Zmienne pomocnicze
  float w_max, w_min;     // Min i Max z wartosci w tablicy pomocniczej
  float W;                //Suma wyrazow maski (kernela) 
  float **mask;           // Tablica pomocnicza

  //Alokowanie pamieci na tablice pomocnicza
  mask=(float**) malloc((obraz->wym_y)*sizeof(float*));
  for(i = 0; i<obraz->wym_y; i++){
    mask[i]=(float*) malloc((obraz->wym_x)*sizeof(float));
  }

  //Glowna czesc funkcji
  for(k = 0; k < obraz->RGB; k++){
    for(i = 1; i < obraz->wym_y-1; i++){  
      for(j = 1; j < obraz->wym_x-1; j++){ 
      
        //Mnozenie odpowiednich wyrazów maski przez odpowiedznie wyrazy z obrazu i sumowanie wyniku
        mask[i][j] = (wybor->kernel[0][0]*obraz->piksele[i-1][j-1][obraz->wybrane_kolory[k]] + wybor->kernel[0][1]*obraz->piksele[i-1][j][obraz->wybrane_kolory[k]] + wybor->kernel[0][2]*obraz->piksele[i-1][j+1][obraz->wybrane_kolory[k]]
                      + wybor->kernel[1][0]*obraz->piksele[i][j-1][obraz->wybrane_kolory[k]] + wybor->kernel[1][1]*obraz->piksele[i][j][obraz->wybrane_kolory[k]] + wybor->kernel[1][2]*obraz->piksele[i][j+1][obraz->wybrane_kolory[k]]
                  + wybor->kernel[2][0]*obraz->piksele[i+1][j-1][obraz->wybrane_kolory[k]] + wybor->kernel[2][1]*obraz->piksele[i+1][j][obraz->wybrane_kolory[k]] + wybor->kernel[2][2]*obraz->piksele[i+1][j+1][obraz->wybrane_kolory[k]]);
        
        //Szukanie wartosci maksymalnej i minimalnej pikseli w obrazie dla poszczegolnych kolorow RGB
        if(j==1&&i==1) {
            w_min=mask[i][j];
            w_max=mask[i][j];
          }

        if(mask[i][j]>w_max){
          w_max=mask[i][j];
        } 
        if(mask[i][j]<w_min){
          w_min=mask[i][j];
        } 
      }
    }  
  

    W = wybor->kernel[0][0] + wybor->kernel[0][1] + wybor->kernel[0][2]    //
      + wybor->kernel[1][0] + wybor->kernel[1][1] + wybor->kernel[1][2]    //Suma wyrazow maski
      + wybor->kernel[2][0] + wybor->kernel[2][1] + wybor->kernel[2][2];   //

    
    if(W == 0){   //Sprawdzanie czy suma wyrazów jest równa 0 
      Znak = 1;
    }

    for(i = 0; i < 3; i++){      
      for(j = 0; j < 3; j++){
        if(wybor->kernel[i][j] < 0){  //Sprawdzanie czy w masce sa wartosci ujemne
          Znak = 1;
        }
      }
    }
  
    //Sprawdzanie warunku normalizacji danych
    if(Znak == 0){  
      for(i = 1; i < obraz->wym_y-1; i++){
        for(j = 1; j < obraz->wym_x-1; j++){
          obraz->piksele[i][j][obraz->wybrane_kolory[k]] = (mask[i][j]/W);  //Normalizacja danych gdy wyrazy maski sa dodatnie
        }                                                                   // i przypisanie do glownej tablicy z pikselami
      }
    }
    else{
      for(i = 1; i < obraz->wym_y-1; i++){
        for(j = 1; j < obraz->wym_x-1; j++){
          obraz->piksele[i][j][obraz->wybrane_kolory[k]] = (((mask[i][j]-w_min)/(w_max-w_min))*(obraz->odcieni));   //Normalizacja danych gdy wyrazy maski sa dodatnie lub ujemne
        }                                                                                                           // i przypisanie do glownej tablicy z pikselami
      }
    }
  } 

  //Zwalnianie pamieci po tablicy pomocniczej 
  for(i = 0; i<obraz->wym_y; i++){
    free(mask[i]);
  }
  free(mask);

  //Zwalnianie pamieci po tablicy maski (kernel)
  for(i = 0; i<3; i++){
    free(wybor->kernel[i]);
  }
  free(wybor->kernel);
  return 1;
}

//Funkcja rozmycia
int rozmycie_pionowe(t_obraz * obraz, t_opcje * wybor){
  int i, j, k, kolor;  //Zmienne pomocnicze
  int *** tab_pom;     //Talica pomocnicza

  //Alokowanie pamieci dla tablicy pomocniczej
  tab_pom = (int ***) malloc((obraz->wym_y)*sizeof(int**));
    for (int i=0;i<obraz->wym_y;i++) {
      tab_pom[i]= (int**) malloc((obraz->wym_x)*sizeof(int*));
      for (int j=0;j<obraz->wym_x;j++) {
        tab_pom[i][j]=(int*)malloc(obraz->kolory*sizeof(int));
      }
    }
 
  for(i = 1; i < (obraz->wym_y - 1); i++){
    for(j = 1; j < (obraz->wym_x -1); j++){
      for(k = 0; k < obraz->RGB; k++){
        //Rozmywanie pionowe i przypisywanie wynikow do tablicy pomocniczej 
        tab_pom[i][j][obraz->wybrane_kolory[k]] = ((obraz->piksele[i-1][j][obraz->wybrane_kolory[k]] + obraz->piksele[i][j][obraz->wybrane_kolory[k]] + obraz->piksele[i+1][j][obraz->wybrane_kolory[k]])/3);   
      }
    }
  }
  for(i = 1; i < (obraz->wym_y - 1); i++){
    for(j = 1; j < (obraz->wym_x - 1); j++){
      for(k = 0; k < obraz->RGB; k++){
        //Przypisywanie wartosci tablicy pomocniczej do tablicy obraz
        obraz->piksele[i][j][obraz->wybrane_kolory[k]] = tab_pom[i][j][obraz->wybrane_kolory[k]];    
      }
    }
  }

  //Zwanianie pamieci po tablicy pomocniczej
  for (int j=0;j<obraz->wym_y;j++) {
    for (int i=0; i<obraz->wym_x;i++) {
      free(tab_pom[j][i]);
    }
    free(tab_pom[j]);
  }
  free(tab_pom); 
  return 1;
}

//Funkcja rozmycia
int rozmycie_poziome(t_obraz * obraz, t_opcje * wybor){
  int i, j, k, kolor;  //Zmienne pomocnicze
  int *** tab_pom;     //Tablica pomocnicza

  //Alokowanie pamieci dla tablicy pomocniczej
  tab_pom = (int ***) malloc((obraz->wym_y)*sizeof(int**));
    for (int i=0;i<obraz->wym_y;i++) {
      tab_pom[i]= (int**) malloc((obraz->wym_x)*sizeof(int*));
      for (int j=0;j<obraz->wym_x;j++) {
        tab_pom[i][j]=(int*)malloc(obraz->kolory*sizeof(int));
      }
    }

  for(i = 1; i < (obraz->wym_y - 1); i++){
    for(j = 1; j < (obraz->wym_x -1); j++){
      for(k = 0; k < obraz->RGB; k++){
        //Rozmywanie poziome i przypisywanie wynikow do tablicy pomocniczej 
        tab_pom[i][j][obraz->wybrane_kolory[k]] = ((obraz->piksele[i][j-1][obraz->wybrane_kolory[k]] + obraz->piksele[i][j][obraz->wybrane_kolory[k]] + obraz->piksele[i][j+1][obraz->wybrane_kolory[k]])/3);   
      }
    }
  }
  for(i = 1; i < (obraz->wym_y - 1); i++){
    for(j = 1; j < (obraz->wym_x - 1); j++){
      for(k = 0; k < obraz->RGB; k++){
        //Przypisywanie wartosci tablicy pomocniczej do tablicy obraz
        obraz->piksele[i][j][obraz->wybrane_kolory[k]] = tab_pom[i][j][obraz->wybrane_kolory[k]];    //Przypisywanie wartosci tablicy pomocniczej do tablicy obraz
      }
    }
  }

  //Zwanianie pamieci po tablicy pomocniczej
  for (int j=0;j<obraz->wym_y;j++) {
    for (int i=0; i<obraz->wym_x;i++) {
    free(tab_pom[j][i]);
    }
    free(tab_pom[j]);
  }
  free(tab_pom); 
  return 1;
}

int R_Histogramu(t_obraz * obraz, t_opcje * wybor){
  //Zmienne pomocnicze
  int i, j, k;
  float Lmax, Lmin ;
  
  //Przypisanie poczatkowych wartosci dla min i max
  Lmax=0;
  Lmin=obraz->odcieni;

  for (i=0; i < obraz->wym_y; i++) {
    for (j=0; j < obraz->wym_x; j++) {
      for(k=0; k < obraz->RGB; k++) {
        //Szukanie wartosci minimalnej i maksymalnej w obrazie
        if (obraz->piksele[i][j][obraz->wybrane_kolory[k]] < Lmin) Lmin=obraz->piksele[i][j][obraz->wybrane_kolory[k]];
        if (obraz->piksele[i][j][obraz->wybrane_kolory[k]] > Lmax) Lmax=obraz->piksele[i][j][obraz->wybrane_kolory[k]];
      }
    }
  }

  for (i=0; i<obraz->wym_y; i++) {
    for (j=0; j<obraz->wym_x; j++) {
      for(k=0; k<obraz->RGB; k++) {
        //Operacja rozciagania histogramu
        obraz->piksele[i][j][obraz->wybrane_kolory[k]] = ((obraz->piksele[i][j][obraz->wybrane_kolory[k]] - Lmin) * ((obraz->odcieni) / (Lmax-Lmin)));
      }
    }
  }
  return 1;
}

//Funkcja zmiany poziomow
int zmiana_poziomow(t_obraz * obraz, int kolorA, int kolorB){
  int i, j, k;  //Zmienne pomocnicze
  //Petla glowna
  for(i=0; i<obraz->wym_y; i++){
    for(j=0; j<obraz->wym_x; j++){
      for(k=0; k<obraz->RGB; k++){
      
        if (obraz->piksele[i][j][obraz->wybrane_kolory[k]]<=kolorA) {   //L(x,y) <= Czern
          obraz->piksele[i][j][obraz->wybrane_kolory[k]]=0; //Zmiana poziomow
        }
        else if (obraz->piksele[i][j][obraz->wybrane_kolory[k]]>=kolorB) {  //L(x,y) >= Biel
          obraz->piksele[i][j][obraz->wybrane_kolory[k]] = obraz->odcieni;  //Zmiana poziomow
        }
        else {  // Biel >= L(x,y) >= Czern
          obraz->piksele[i][j][obraz->wybrane_kolory[k]] = ((obraz->piksele[i][j][obraz->wybrane_kolory[k]]-kolorA))*(obraz->odcieni/(kolorB-kolorA)); //Zmiana poziomow
        }
      }
    }
  }
  return 1;
}