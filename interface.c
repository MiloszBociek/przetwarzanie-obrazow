#include "interface.h"
#include "struct.h"
#include "I_O.h"
#include "filtry.h"

#include<stdlib.h>
#include<stdio.h>
#include<string.h>
#define W_OK 1                   /* wartosc oznaczajaca brak bledow */
#define B_NIEPOPRAWNAOPCJA -1    /* kody bledow rozpoznawania opcji */
#define B_BRAKNAZWY   -2
#define B_BRAKWARTOSCI  -3
#define B_BRAKPLIKU   -4

#define B_POWTORZENIEKOLORU   -6
#define B_NIEPOPRAWNAWARTOSC  -5

/************************************************************************/
/*  Szczegolowy opis bledow znajduje sie w pliku "manual.txt" */
/************************************************************************/

/************************************************************************/
/* Funkcja rozpoznaje opcje wywolania programu zapisane w tablicy argv  */
/* i zapisuje je w strukturze wybor                                     */
/* Skladnia opcji wywolania programu                                    */
/*         program {[-i nazwa] [-o nazwa] [-p liczba] [-n] [-r] [-d] }  */
/* Argumenty funkcji:                                                   */
/*         argc  -  liczba argumentow wywolania wraz z nazwa programu   */
/*         argv  -  tablica argumentow wywolania                        */
/*         wybor -  struktura z informacjami o wywolanych opcjach       */
/* PRE:                                                                 */
/*      brak                                                            */
/* POST:                                                                */
/*      funkcja otwiera odpowiednie pliki, zwraca uchwyty do nich       */
/*      w strukturze wybór, do tego ustawia na 1 pola dla opcji, ktore  */
/*	poprawnie wystapily w linii wywolania programu,                     */
/*	pola opcji nie wystepujacych w wywolaniu ustawione sa na 0;         */
/*	zwraca wartosc W_OK (0), gdy wywolanie bylo poprawne                */
/*	lub kod bledu zdefiniowany stalymi B_* (<0)                         */
/* UWAGA:                                                               */
/*      funkcja nie sprawdza istnienia i praw dostepu do plikow         */
/*      w takich przypadkach zwracane uchwyty maja wartosc NULL         */
/************************************************************************/

int przetwarzaj_opcje(int argc, char **argv, t_opcje *wybor, t_obraz *obraz) {
  
  int i, prog, kolor;
  char *nazwa_pliku_we, *nazwa_pliku_wy;
  wybor->nazwa_we=malloc(128*sizeof(char));
  wybor->nazwa_wy=malloc(128*sizeof(char));

  strcpy(wybor->nazwa_wy,"stdout");          /* na wypadek gdy nie podano opcji "-o" */

  for (i=1; i<argc; i++) {
    if (argv[i][0] != '-')                   /* blad: to nie jest opcja - brak znaku "-" */
      return B_NIEPOPRAWNAOPCJA; 
    switch (argv[i][1]) {
      case 'i': {                            /* opcja z nazwa pliku wejsciowego */
        if (++i<argc) {                      /* wczytujemy kolejny argument jako nazwe pliku */
          nazwa_pliku_we=argv[i];
          if (strcmp(nazwa_pliku_we,"-")==0){       /* gdy nazwa jest "-"        */      
            wybor->plik_we=stdin;                   /* ustwiamy wejscie na stdin */  
            strcpy(wybor->nazwa_we,"stdin");
          }
          else{                                     /* otwieramy wskazany plik   */
            wybor->plik_we=fopen(nazwa_pliku_we,"r");
            strcpy(wybor->nazwa_we, nazwa_pliku_we);
          }    
        } 
        else{ 
        return B_BRAKNAZWY;      /* blad: brak nazwy pliku */
        }
        if(strcmp(wybor->nazwa_we, "")!=0){
          if(czytaj(wybor->plik_we, obraz)<0){
            return B_BRAKPLIKU;
          }
        }
        break;
      }
      case 'o': {                   /* opcja z nazwa pliku wyjsciowego */
        if (++i<argc) {             /* wczytujemy kolejny argument jako nazwe pliku */
          nazwa_pliku_wy=argv[i];
          if (strcmp(nazwa_pliku_wy,"-")==0){   /* gdy nazwa jest "-"         */
            wybor->plik_wy=stdout;              /* ustwiamy wyjscie na stdout */
            strcpy(wybor->nazwa_wy,"stdout");
          }
          else{                                 /* otwieramy wskazany plik    */   
            strcpy(wybor->nazwa_wy, nazwa_pliku_wy);
          }     
        } 
        else{
            return B_BRAKNAZWY;                 /* blad: brak nazwy pliku */
        } 
        break;
      }
      case 'p': {
        if (++i<argc) {                           /* wczytujemy kolejny argument jako wartosc progu */
          if (sscanf(argv[i],"%d",&prog)==1) {    //Jezeli wczytano 1 argument
            if(prog > 100 || prog < 0){           // Czy wartosc progu jest poprawna?
              return B_NIEPOPRAWNAOPCJA;  
            }
            wybor->w_progu=prog;                  //Przyekazywanie wartosci progu do struktury
            progowanie(obraz, wybor);             //Wywlanie funkcji progownaia
          } 
          else{
            return B_NIEPOPRAWNAWARTOSC;          /* blad: niepoprawna wartosc progu */
          }
        } 
        else{
          return B_BRAKWARTOSCI;                  /* blad: brak wartosci progu */
        }  
        break;
      }
      case 'n': { //Wywolanie funkcji negatyw
        negatyw(obraz, wybor);
        break;
      }
      case 'k': {   //Wywolanie funkcji konturowanie
        konturowanie(obraz, wybor);
        break;
      }
      case 'd': {   //Wywolanie funkcji wyswielt (a przed tym zapisz)

        zapisz(wybor, obraz);
        wyswietl(wybor); 
        
        break;
      }
      case 'r': {               //Wywolanie funkcji rozmycia poziomego lub pionowego
        switch (argv[i][2]) {   /* sprawdzanie drugiego parametru w celu okreslenia typu rozmycia */
          case 'x': { //rozmycie poziome
            rozmycie_poziome(obraz, wybor); 
            break;
          }
          case 'y': { //rozmycie pionowe
            rozmycie_pionowe(obraz, wybor);
            break;
          }
          default : { //Przy blednej lub niepodanej opcji
            return B_NIEPOPRAWNAOPCJA;  /* nierozpoznana opcja */
          }                  
        }
        break;
      }
      case 'm': { // Wybor danego koloru
        obraz->RGB=0;
        int R=1, G=1, B=1;
        if (++i<argc) { //Sprawdzanie czy zostal podany argument
          for (int j=0;j<argv[i][j];j++) { /* petla z przelacznikami kolorow funkcji -m */
            if(obraz->RGB<3){
              switch(argv[i][j]) {
                case 'r': { //Wybor koloru czerwonego
                  if(R){  //Wybor czerwonego (tylko raz)
                    obraz->wybrane_kolory[j]=0;
                    obraz->RGB++;
                    R--;
                  }            
                  break;
                }
                case 'g': { //Wybor koloru zielonego
                  if(G){  //Wybor zielonego (tylko raz)
                    obraz->wybrane_kolory[j]=1;   
                    obraz->RGB++; 
                    G--;
                  }       
                  break;
                }
                case 'b': { //Wybor koloru niebieskiego
                  if(B){  //Wybor niebieskiego (tylko raz)
                  obraz->wybrane_kolory[j]=2;
                  obraz->RGB++;
                  B--;
                  }
                  break;
                }
                case 's': { //Konwersja do szarosci
                  if(j==0){  //przelacnik ten musi byc podany na pierwsyzm miejscu inaczej niepoprawna opcja
                    if(obraz->kolory==3){ //Jezeli obraz ppm
                      obraz->kolory=1;
                      obraz->RGB=1;
                      konwersja(obraz);
                    }
                  }else {
                    return B_NIEPOPRAWNAOPCJA;
                  }     
                  break;
                }
                default : {   //W przypadku niepoprawnej opcji
                  return B_NIEPOPRAWNAOPCJA;
                }
              }
            }
          }
        } 
        else{
          return B_BRAKWARTOSCI;             
        } 
        break;
      }
      case 's': {
        int pam;  //Zmienna pomocnicza
        for(pam = 0; pam<9; pam++){ //Sprawdzanie liczby podanych parametrow w filtrze splotu (poprwana ilosc: 9)
          if(!(++i<argc)){
            return B_BRAKWARTOSCI;
          }
        } 
        if(pam==9){ //Jezeli podano poprawna ilosc parametrow
          //Alokacja pamieci na tablice maski
          wybor->kernel=(float**) malloc(3*sizeof(float*));
          for(int j=0; j<3; j++){
            wybor->kernel[j]=(float*) malloc(3*sizeof(float));
          }
        }
        int j=8;  //Zmienna pomocnicza
        for(int k=0; k<3; k++){
          for(int w=0; w<3; w++){
            sscanf(argv[i-j],"%f",&wybor->kernel[k][w]);  //Uzupelnianie tablicy maski (kernel)
            j--;
          }
        }
        maska(obraz, wybor);  //Wywoalnie filtru maski/splotu
        break;
      }
      case 'h': { //Wybranie opcji rozciagania histogramu
        R_Histogramu(obraz, wybor);
        break;
      }
      case 'z': { //Wybranie opcji zmiany poziomow

        int kolorA, kolorB; //Zmienne pomocnicze
        if(++i<argc){     //
          if(++i<argc){   //2 petle sprawdzajace czy podano 2 parametry dla funkcji 
            sscanf(argv[i-1], "%d", &kolorA); //
            sscanf(argv[i], "%d", &kolorB);   //  Pobieranie wartosci parametrow

            if(kolorB > kolorA && kolorB >= 0 && kolorA >= 0 && kolorB <= obraz->odcieni && kolorB <= obraz->odcieni){  //Sprawdzanie warunkow poprawnosci danych wejsciowych
              zmiana_poziomow(obraz, kolorA, kolorB);
            }
            else{ //Niepoprwana opcja
              return B_NIEPOPRAWNAOPCJA;
            }
          }
        }
        break;
      }
      default: {                    // nierozpoznana opcja
        return B_NIEPOPRAWNAOPCJA;
      } 
    } /*koniec switch */
  } /* koniec for */

  if (strcmp(wybor->nazwa_we,"")!=0){  /* ok: wej. strumien danych zainicjowany */ 
    zapisz(wybor, obraz);
    return W_OK;
  }     
  else 
  manual();
    return B_BRAKPLIKU;         /* blad:  nie otwarto pliku wejsciowego  */
}

