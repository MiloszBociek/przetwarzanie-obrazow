#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include"I_O.h"
#include"interface.h"

int czytaj(FILE *plik_we, t_obraz *obraz){
  //printf("FILE FILE FILE: %p \n", plik_we);
  char buf[DL_LINII];      /* bufor pomocniczy do czytania naglowka i komentarzy */
  int znak;                /* zmienna pomocnicza do czytania komentarzy */
  int koniec=0;            /* czy napotkano koniec danych w pliku */
  int i,j;

  /*Sprawdzenie czy podano prawidłowy uchwyt pliku */
  if (plik_we==NULL) {
    fprintf(stderr,"Blad: Nie podano uchwytu do pliku\n");
    return(0);
  }

  /* Sprawdzenie "numeru magicznego" - powinien być P2 */
  if (fgets(buf,DL_LINII,plik_we)==NULL)   /* Wczytanie pierwszej linii pliku do bufora */
    koniec=1;                              /* Nie udalo sie? Koniec danych! */
  if ( (buf[0]!='P') || ((buf[1]!='2')&&(buf[1]!='3')) || koniec) {  /* Czy jest magiczne "P2"? */
    fprintf(stderr,"Blad: To nie jest plik PGM\n");
    return(0);
  }

  if(buf[1]=='2'){  //Jezeli obraz pgm
    obraz->kolory = 1;  //Liczba kolorow
    obraz->RGB = 1;     //Liczba wybranych kolorow (skoro pgm to = 1)
    obraz->wybrane_kolory=malloc(sizeof(int));  //Dynamiczna tabela na wybrane kolory (w tym przypadku 1 element)
    obraz->wybrane_kolory[0]=0; //Skoro plik pgm to nie potrzeba 3 wymiaru (tablica 2D)
  }
  else if(buf[1]=='3'){ //Jezeli obraz ppm
    obraz->kolory = 3;  //Liczba kolorow
    obraz->RGB = 3;     //Liczba wybranych (domyslnie 3 dla ppm (na wypadek braku parametru po -m))

    //Alokowanie tablicy na wybrane kolory (uzywane przy przelaczniku -m do rozrozniania kolorow) 
    obraz->wybrane_kolory=malloc(3*sizeof(int));
    for(int k=0; k<3; k++){
      obraz->wybrane_kolory[k]=k; //kazdy z kolorow ma przypisana swoja wartosc R=0 G=1 B=2
    }
  }

  /* Pominiecie komentarzy */
  do {
    if ((znak=fgetc(plik_we))=='#') {         /* Czy linia rozpoczyna sie od znaku '#'? */
      if (fgets(buf,DL_LINII,plik_we)==NULL)  /* Przeczytaj ja do bufora                */
	koniec=1;                   /* Zapamietaj ewentualny koniec danych */
    }  else {
      ungetc(znak,plik_we);                   /* Gdy przeczytany znak z poczatku linii */
    }                                         /* nie jest '#' zwroc go                 */
  } while (znak=='#' && !koniec);   /* Powtarzaj dopoki sa linie komentarza */
                                    /* i nie nastapil koniec danych         */


  /* Pobranie wymiarow obrazu */
  if (fscanf(plik_we,"%d %d ",&obraz->wym_x,&obraz->wym_y)!=2) {
    fprintf(stderr,"Blad: Brak wymiarow obrazu\n");
    return(0);
  }              


  // ALOKACJA PAMIECI na obraz

  obraz->piksele= (int ***) malloc((obraz->wym_y)*sizeof(int**));
  for (int i=0;i<obraz->wym_y;i++) {
    obraz->piksele[i]= (int**) malloc((obraz->wym_x)*sizeof(int*));
    for (int j=0;j<obraz->wym_x;j++) {
      obraz->piksele[i][j]=(int*)malloc(obraz->kolory*sizeof(int));
    }
  } 

  /* Pobieranie liczby odcieni szarosci */
  if (fscanf(plik_we," %d",&obraz->odcieni)!=1) {
    fprintf(stderr,"Blad: Brak liczby odcieni szarosci\n");
    return(0);
  }
  //Wczytywanie obrazu do pamieci
  for (i=0;i<obraz->wym_y;i++) {
    for (j=0;j<obraz->wym_x;j++) {
      for(int k=0; k<obraz->kolory; k++) {
        if (fscanf(plik_we,"%d",&(obraz->piksele[i][j][k]))!=1) {  //sprawdzanie poprawnosci podanych wymiarow 
	        fprintf(stderr,"Blad: Niewlasciwe wymiary obrazu XXX \n");
        }
      }
    }
  }
  return obraz->wym_x*obraz->wym_y;   /* Czytanie zakonczone sukcesem    */
                                      /* Zwroc liczbe wczytanych pikseli */

}



int zapisz(t_opcje * wybor, t_obraz *obraz){

  int i, j;       //zmienne pomocnicze

  //Otwieranie pliku i przekazywanie uchwytu do struktury t_opcje
  wybor->plik_wy=fopen(wybor->nazwa_wy,"w");  

    //Zapisywanie informacji o obrazie 
    if(obraz->kolory == 3){ //Obraz ppm
      fprintf(wybor->plik_wy, "P3\n");  
    }
    else{ //Obraz pgm
      fprintf(wybor->plik_wy, "P2\n");
    }
    
    fprintf(wybor->plik_wy, "%d %d\n%d\n", obraz->wym_x, obraz->wym_y, obraz->odcieni);
   //Zapisywanie poszczegolnych pikseli
    for(i=0; i<obraz->wym_y; i++){
      for(j=0; j<obraz->wym_x; j++){
        for(int k = 0; k<obraz->kolory; k++){
          fprintf(wybor->plik_wy, "%3d ", obraz->piksele[i][j][k]);
        } 
      }
      fprintf(wybor->plik_wy,"\n");  // Dla ladniejszego wygladu pliku wyjsciowego
    }  
    //Zamykanie pliku
    fclose(wybor->plik_wy);
  //Zwracanie wymiarow
  return obraz->wym_x*obraz->wym_y;  
   
}


/* Wyswietlenie obrazu o zadanej nazwie za pomoca programu "display"   */
int wyswietl(t_opcje * wybor) {
  char polecenie[DL_LINII];      /* bufor pomocniczy do zestawienia polecenia */
  strcpy(polecenie,"display ");  /* konstrukcja polecenia postaci */
  strcat(polecenie,wybor->nazwa_wy);     /* display "nazwa_pliku" &       */
  strcat(polecenie," &");
  system(polecenie);
  for(int i=0;i<32000;i++) {          /* dealay dla poprawnego dzialania komendy display */
        for (int j=0;j<20000;j++) {}
  }
return 1;
}

void manual() {
  char polecenie[DL_LINII];      /* bufor pomocniczy do zestawienia polecenia */
  strcpy(polecenie,"cat manual.txt");  /* konstrukcja polecenia postaci */
  printf("%s\n",polecenie);      /* wydruk kontrolny polecenia */
  system("clear");
  system(polecenie);             /* wykonanie polecenia        */
}