/* SPRAWOZDANIE znajduje się pod funkcja main */

#include<stdio.h>
#include<string.h>
#include<stdlib.h>

#include "I_O.h"
#include "filtry.h"
#include "interface.h"

int main(int argc, char **argv) {
  int Wynik;
  t_obraz obraz;
  t_opcje opcje;
  
  //Przetwarzanie opcji podanych przy uruchamianiu programu w terminalu
  Wynik = przetwarzaj_opcje(argc, argv, &opcje, &obraz);

  if(Wynik == -4){
    fprintf(stderr, "NIE MA TAKIEGO PLIKU \n");
    return 0;
  }
  else if(Wynik == -1){
    fprintf(stderr, "NIEPOPRAWNA OPCJA\n");
    return 0;
  }
  else if(Wynik == -3){
    fprintf(stderr, "BRAK WARTOSCI \n");
  }
  else if(Wynik == -5){
    fprintf(stderr, "NIEPOPRAWNA WARTOSC \n");
  }

  if (obraz.kolory==3 || obraz.kolory==1) {   /* zwalnienie pamieci nastapi tylko wtedy, gdy zostal podany plik wejsciowy */

    /* Zwalnianie pamieci dla obrazu oraz dla pomocniczych nazw pliku i wybranych kolorow*/
    for (int j=0;j<obraz.wym_y;j++) {
      for (int i=0; i<obraz.wym_x;i++) {
      free(obraz.piksele[j][i]);
      }
      free(obraz.piksele[j]);
    }
    free(obraz.wybrane_kolory);
    free(obraz.piksele); 
  }
  if(strcmp(opcje.nazwa_we,"")!=0) {
    free(opcje.nazwa_we);
    free(opcje.nazwa_wy);
  }
  return 0;
}

/*

  SPRAWOZDANIE

  Milosz Bociek
  Data: 11.01.2021

  Opis programu:
    Program przetwarza obrazy ppm oraz pgm (P2 i P3). Uzywa do tego dynamicznie alokowanych tablic.
    Ma też interfejs liniowy (polecenia są wykonywane w kolejnosci w jakiej zostaly podane podczas wywolania).

    !UWAGA!
    Na koncu funkcji "wyswietl" w pliku I_O.c znajduje sie podwojna petla for ktora jest pusta. 
    Spowodowane jest to tym ze przy wiekszej ilosci operacji lub przy wiekszych obrazach polecenie
    "display" nie funkcjonuje poprawnie w tym programie. 

  Kompilacja:
    Program kompiluje sie przy uzyciu make'a.
    Wystarczy wpisac "make" w folderze z programem.

  Wywolanie:
    Opisane jest bardziej szczegolowo w pliku "manual.txt"
    Plik ten mozna wypisac na standardowe wyjscie komenda "./OOBRAZY2.out"

    Szybkie przedstawienie wywolania: 

      ./OBRAZY2 -i <nazwa_pliku_wejsciowego> -o <nazwa_pliku_wyjsciowego> -d
  
    Program wtedy wyswietli plik a takze go zapisze w pliku docelowym
    Opcje przetwarzania znajduja sie w pliku "manual.txt"


  TESTY:

  Adnotacja:
    Program byl testowany na obrazach ppm i pgm z katalogu /home/mucha/edu/info2/obrazki znajdujacego sie na Diablo.
    Testowanie odbywalo sie poprzez porownywanie tablic wyikowych pliku wejsciowego oraz wyjsciowego a takze przez porownywanie
    z tymi utworzonymi komneda "convert" (ImageMagick)


    Test 1:
      SPRAWDZA: 
        Wlaczenie programu bez zadnego parametru funkcji
      ZALOZENIA: 
        plik "manual.txt" zostanie wyswietlony na standardowym wyjsciu
      WYNIK: 
        Wyswietlil sie plik "manual.txt"
      WNIOSEK: 
        Poprawne dzialanie funkcji rozpoznajacej przelaczniki funkcji
        oraz funkcji uruchamiajacej instrukcje dla uzytkownika
    
    Test 2:
        SPRAWDZA: 
          Wlaczenie programu z blednym przelacznikiem funkcji
        ZALOZENIA: 
          Powinien zostac zwrocony odpowiedni kod bledu
        WYNIK: 
          Program zwrocil poprawny kod bledu
        WNIOSEK: 
          Poprawne dzialanie funkcji rozpoznajacej przelaczniki funkcji

    Test 3
        SPRAWDZA: 
          Podanie przelacznika funkcji, ktory wymaga wartosci bez wartosci
        ZALOZENIA: 
          Powinien zostac zwrocony odpowiedni kod bledu
        WYNIK: 
          Program zwrocil poprawny kod bledu
        WNIOSEK: 
          Poprawne dzialanie funkcji rozpoznajacej przelaczniki funkcji
      
    Test 4
        SPRAWDZA: 
          Wlaczenie programu podajac obrazy z rozszerzniem *.pgm oraz *.pgm
        ZALOZENIA: 
          Program powinien poprawnie zidentyfokowac typ
          obrazu i nastepnie poprawnie wykonac na nim filtry
        WYNIK: 
          Program rozpoznaje typ obrazu i prawidlowo przekazuja ta 
          informacje do nastepnych funkcji
        WNIOSEK: 
          Poprawne dzialanie programu na roznych typach obrazu

    Test 5
        SPRAWDZA: 
          Wywolanie funkcji, ktore nie potrzebuja dodatkowego parametru
        ZALOZENIA: 
          Obraz powinien zostac poprawnie przetworzony
        WYNIK: 
          Obraz zostaje poprawnie przetworzony
        WNIOSEK: 
          Poprawne dzialanie funkcji 

    Test 6.0
        SPRAWDZA: 
          Wywolanie funkcji progowania z wartoscia mieszczaca sie w
          zaskresie [0;100]
        ZALOZENIA: 
          Obraz powinien zostac przetworzony
        WYNIK: 
          Obraz zostaje prawidlowo przetworzony
        WNIOSEK: 
          Poprawne rozpoznawanie wartosci parametru progowania

    Test 6.1
        SPRAWDZA: 
          Wywolanie funkcji progowania z wartoscia z poza zaskresu [0;100]
        ZALOZENIA: 
          Powinien zostac zwrocony odpowiedni kod bledu
        WYNIK: 
          Program zwrocil poprawny kod bledu
        WNIOSEK: 
          Poprawne rozpoznanie przez program wartosci parametru oraz
          jego analiza 

    Test 7.0
        SPRAWDZA: 
          Wywolanie funkcji zmiana poziomow, gdzie wartosc czerni jest 
          mniejsza od bieli oraz mieszcza sie one w zakresie [0;wybor->odcieni]
        ZALOZENIA: 
          Obraz powinien zostac przetworzony
        WYNIK: 
          Obraz zostaje prawidlowo przetworzony
        WNIOSEK: 
          Poprawne rozpoznawanie wartosci parametrow zmiany poziomow

    Test 7.1
        SPRAWDZA: 
          Wywolanie funkcji zmiany poziomow z wartosciami z poza
          zaskresu [0;wybor->odcieni]
        ZALOZENIA: 
          Powinien zostac zwrocony odpowiedni kod bledu
        WYNIK: 
          Program zwrocil poprawny kod bledu
        WNIOSEK: 
          Poprawne rozpoznanie przez program wartosci parametrow oraz
          ich analiza

    Test 7.2
        SPRAWDZA: 
          Wywolanie funkcji zmiany poziomow, gdzie wartosc czerni jest
          wieksza od bieli 
        ZALOZENIA: 
          Powinien zostac zwrocony odpowiedni kod bledu
        WYNIK: 
          Program zwrocil poprawny kod bledu
        WNIOSEK: 
          Poprawne rozpoznanie przez program wartosci parametrow oraz
          ich analiza (w tym przypadku zakres wartosci nie ma znaczenia)

    Test 8.0
        SPRAWDZA: 
          Wywolanie funkcji maski z 9 parametrami
        ZALOZENIA: 
          Obraz powinien zostac przetworzony
        WYNIK: 
          Po wywolaniu funkcji "maska" wynikowy plik jest podobny/zostal
          utworzony na podobnej zasadzie, jak przykladowe dzialanie funkcji
          splotu dostepne w bibliografii do opisu funkcji znajdujacej sie na
          stronie kursu :
          *TADEUSIEWICZ R., KOROHODA P., Komputerowa analiza i pretwarzanie
          obrazów, Wydawnictwo fundacji Postępu Telekomunikacji, Kraków 1997,
          str. 83–109*
        WNIOSEK: 
          Wynik programu wydaje sie byc prawidlowy

    Test 8.1
        SPRAWDZA: 
          Wywolanie funkcji maski z wiecej niz 9 parametrami
        ZALOZENIA: 
          Powinien zostac zwrocony odpowiedni kod bledu
        WYNIK: 
          Program zwrocil kod bledu odpowiadajacy za niepoprawny  przelacznik
        WNIOSEK: 
          Nadmiarowa ilosc danych nie jest brana pod uwage dzialania
          funkcji, jednakze wplywa ona na funkcjonowanie calego programu
    
    Test 8.2
        SPRAWDZA: 
          Wywolanie funkcji maski z mniej niz 9 parametrami
        ZALOZENIA: 
          Powinien zostac zwrocony odpowiedni kod bledu
        WYNIK: 
          Program zwrocil kod bledu
        WNIOSEK: 
          Poprawne sprawdzanie ilosci podanych parametrow

    Test 9.0
        SPRAWDZA: 
          Wywolanie funkcji operacji na kolorach bez podania zadnego koloru
        ZALOZENIA: 
          Powinien zostac zwrocony odpowiedni kod bledu
        WYNIK: 
          Program zwrocil kod bledu
        WNIOSEK: 
          Program prawidlowo sprawdzil czy zostal podany jakis kolor

    Test 9.1
        SPRAWDZA: 
          Wywolanie funkcji operacji na kolorach podajac 1 lub 2 wartosci 
          ([r][g][b]) - nie powtarzajac ich oraz jednej z funkcji
        ZALOZENIA: 
          Po podaniu odpowiednij funkcji obraz powinien
          zostac przetworzony w oparciu o podane kolory
        WYNIK: 
          Obraz zostaje prawidlowo przetworzony
        WNIOSEK: 
          Poprawne rozpoznanie przez program na jakich kolorach ma operowac

    Test 9.2
        SPRAWDZA: 
          Wywolanie funkcji operacji na kolorach podajac wiecej niz 3 kolory
          lub powtarzajac je lub dodajac "s" odpowiednie za konwersje do
          szarosci
        OCZEKIWANY REZULTAT: 
          Powinien zostac zwrocony odpowiedni kod bledu lub zignorowac niepoprawne dane
        WYNIK: 
          Program zwraca kod bledu przy podaniu <kolor>s
          Przy podaniu s<kolor> nastepuje jedynie konwersja do szarosci
          Przy podaniu wiecej niz 3 kolorow dalsze wartosci sa ignorowane
        WNIOSEK: 
          Program prawidlowo rozpoznaje nadmiar oraz powielenie kolorow
    
    Test 9.3
        SPRAWDZA: 
          Wywolanie funkcji operacji na kolorach podajac "s" jako kolor
          -konwersja do szarosci
        ZALOZENIA: 
          Obraz powinien zostac przetworzony 
        WYNIK: 
          Program prawidlowo przetworzyl obraz
        WNIOSEK: 
          Funkcja rozpoznaje przelacznik/kolor "s" a nastepnie
          prawidlowo przeprowadza konwersje do szarosci na wszystkich
          poziomach


---WNIOSEK KONCOWY---

Program dziala poprawnie
*/  