#ifndef INTERFACE2_H
#define INTERFACE2_H

#include <stdio.h>
#include "struct.h"

//Struktura ktora przechowuje wybrane opcje
typedef struct {
  char * nazwa_we, * nazwa_wy;    //Nazwy plikow wej/wyj
  FILE *plik_we, *plik_wy;        // uchwyty do pliku wej. i wyj.
  int w_progu;                    // wartosc progu dla opcji progowanie
  int kolory, RGB;                // kolejno: liczba kolorow w obrazie, liczba wybranych kolorow (przy opcji -m) 
  float **kernel;                 // tablica maski
} t_opcje;


// Funkcja przetwarzania wyborow (opcji)
int przetwarzaj_opcje(int argc, char **argv, t_opcje *wybor, t_obraz *obraz);

#endif