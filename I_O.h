#ifndef I_O_H
#define I_O_H

#include"struct.h"
#include"interface.h"

#define MAX 512            /* Maksymalny rozmiar wczytywanego obrazu */
#define DL_LINII 1024      /* Dlugosc buforow pomocniczych */

//int czytaj(FILE *plik_we,int obraz_pgm[][MAX],int *wymx,int *wymy, int *szarosci);

int czytaj(FILE *plik_we,t_obraz * obraz);
int zapisz(t_opcje * wybor,t_obraz * obraz1);
int wyswietl(t_opcje * wybor);
void manual();

#endif
